package com.arutalaautomation;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Test_AutomationTest {

    public WebDriver driver;
    public String webUrl = "http://automationpractice.com";
    public String drvPath = "D:\\AB\\chromedriver.exe";


    @BeforeTest
    public void launchBrowser(){
        System.setProperty("webdriver.chrome.driver", drvPath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(webUrl);
    }

    @BeforeMethod
    public void verifyHomepage(){
        String currentUrl = driver.getCurrentUrl();
        if (currentUrl != "automationpractice.com/index.php"){
            driver.get(webUrl);
        }
    }

    @Test(priority = 0)
    public void registerPage() throws InterruptedException {

        ///Register Page

        driver.findElement(By.xpath("//*[@class='login']")).click();
        Thread.sleep(3000);
        WebElement createAccount = driver.findElement(By.id("email_create"));

        ///Input your own email. But for the sake of this test, let's use a random email instead
        ///Since we need new random email before running the script, don't forget to change the email field here and at line 335
        createAccount.sendKeys("lee@doo.com");
        createAccount.submit();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

        ///Input your own personal information. But for the sake of this sake, let's enter a random data instead

        ///Radio button gender selection, uniform-id_gender1 for male, change to uniform-id_gender2 for female
        WebElement genderRadio = driver.findElement(By.id("uniform-id_gender1"));

        WebElement firstName = driver.findElement(By.id("customer_firstname"));
        WebElement lastName = driver.findElement(By.id("customer_lastname"));
        WebElement password = driver.findElement(By.name("passwd"));

        ///Dropdown for the Date of Birth
        Select dayDropDown = new Select(driver.findElement(By.name("days")));
        Select monthDropDown = new Select(driver.findElement(By.name("months")));
        Select yearDropDown = new Select(driver.findElement(By.name("years")));

        ///Checkbox
        WebElement newsletterCheckBox = driver.findElement(By.id("uniform-newsletter"));
        WebElement offersCheckBox = driver.findElement(By.id("uniform-optin"));

        ///Address Form
        WebElement fNameAddress = driver.findElement(By.id("firstname"));
        WebElement lNameAddress = driver.findElement(By.id("lastname"));
        WebElement company = driver.findElement(By.id("company"));
        WebElement address1 = driver.findElement(By.id("address1"));
        WebElement address2 = driver.findElement(By.id("address2"));
        WebElement city = driver.findElement(By.id("city"));
        Select stateSelect = new Select(driver.findElement(By.id("id_state")));
        WebElement zipcode = driver.findElement(By.id("postcode"));
        Select countrySelect = new Select(driver.findElement(By.id("id_country")));
        WebElement other = driver.findElement(By.id("other"));
        WebElement homePhone = driver.findElement(By.id("phone"));
        WebElement mobilePhone = driver.findElement(By.id("phone_mobile"));
        WebElement alias = driver.findElement(By.id("alias"));

        ///This is where you input the form. You can edit inside the "" or leave it

        genderRadio.click();
        firstName.sendKeys("John");
        lastName.sendKeys("Doe");
        password.sendKeys("password");

        dayDropDown.selectByValue("1");
        monthDropDown.selectByValue("1");
        yearDropDown.selectByValue("1991");

        fNameAddress.sendKeys("John");
        lNameAddress.sendKeys("Doe");
        company.sendKeys("Doe-Jo");
        address1.sendKeys("Allan Ave");
        address2.sendKeys("3101");
        city.sendKeys("Sacramento");
        stateSelect.selectByValue("5");
        zipcode.sendKeys("95691");
        ///Since the country dropdown only has United States, you can leave it or input the value as "21"
        countrySelect.selectByValue("21");
        //You can leave this form blank, but for now let's input a random text
        other.sendKeys("Lorem Ipsum Dolor Sit Amet");
        homePhone.sendKeys("234432");
        mobilePhone.sendKeys("123321");
        alias.clear();
        alias.sendKeys("Home");

        ///Submit all data in the form
        driver.findElement(By.id("submitAccount")).click();
        ///Wait for the page to load, and then signout
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

    }

    @Test
    public void searchAndCatalogPage() throws InterruptedException {

        ///Locating searchbar
        WebElement searchbar1 = driver.findElement(By.name("search_query"));
        ///Sample searchterm, putting items that don't exist
        searchbar1.sendKeys("abc");
        searchbar1.submit();
        Thread.sleep(4000);

        ///Since abc doesn't exit, now we put something on sale in this website
        WebElement searchbar2 = driver.findElement(By.id("search_query_top"));
        searchbar2.clear();
        searchbar2.sendKeys("blouse");
        searchbar2.submit();

        ///Clicking on the product, and interact with the page
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id=\"center_column\"]/ul/li/div")).click();
        /*
        ///Zoom in on the picture
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("bigpic")).click();

        ///Cycle through the product's pictures
        driver.findElement(By.xpath("//*[@class='fancybox-nav fancybox-prev']")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@class='fancybox-nav fancybox-next']")).click();


        ///Close the picture
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@class='fancybox-item fancybox-close']")).click();

        ///Share this product to social media
        WebElement twitter = driver.findElement(By.xpath("//*[@id=\"center_column\"]/div/div/div[3]/p[7]/button[1]"));
        WebElement facebook = driver.findElement(By.xpath("//*[@id=\"center_column\"]/div/div/div[3]/p[7]/button[2]"));
        WebElement gPlus = driver.findElement(By.xpath("//*[@id=\"center_column\"]/div/div/div[3]/p[7]/button[3]"));
        WebElement pinterest = driver.findElement(By.xpath("//*[@id=\"center_column\"]/div/div/div[3]/p[7]/button[4]"));


        String mainWindow = driver.getWindowHandle();
        twitter.click();
        for (String twitWindow : driver.getWindowHandles()){
            driver.switchTo().window(twitWindow);
        }

        Thread.sleep(3000);
        driver.close();
        driver.switchTo().window(mainWindow);

        facebook.click();
        for (String fbWindow : driver.getWindowHandles()){
            driver.switchTo().window(fbWindow);
        }

        Thread.sleep(3000);
        driver.close();
        driver.switchTo().window(mainWindow);

        gPlus.click();
        for (String gpWindow : driver.getWindowHandles()){
            driver.switchTo().window(gpWindow);
        }

        Thread.sleep(3000);
        driver.close();
        driver.switchTo().window(mainWindow);

        pinterest.click();
        for (String pinWindow : driver.getWindowHandles()){
            driver.switchTo().window(pinWindow);
        }

        Thread.sleep(3000);
        driver.close();
        driver.switchTo().window(mainWindow);

        ///Share directly to friend's email
        driver.findElement(By.id("send_friend_button")).click();
        driver.findElement(By.id("friend_name")).sendKeys("Boo Hoo");
        driver.findElement(By.id("friend_email")).sendKeys("HooBoo@boo.com");
        Thread.sleep(2000);
        driver.findElement(By.id("sendEmail")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id=\"product\"]/div[2]" + "/div" + "/div" + "/div/p[2]/input")).click();
         */



        ///Product quantity, size, and color
        WebElement qtyBox = driver.findElement(By.name("qty"));
        WebElement qtyDecrease = driver.findElement(By.xpath("//*[@class='icon-minus']"));
        WebElement qtyIncrease = driver.findElement(By.xpath("//*[@class='icon-plus']"));
        Select sizeSelector = new Select(driver.findElement(By.name("group_1")));
        WebElement whiteColorSelection = driver.findElement(By.name("White"));
        WebElement blackColorSelection = driver.findElement(By.name("Black"));
        WebElement submitProduct = driver.findElement(By.name("Submit"));

        ///Input the value for the order form here
        qtyBox.clear();
        qtyBox.sendKeys("2");
        qtyIncrease.click();
        qtyDecrease.click();
        qtyDecrease.click();
        sizeSelector.selectByValue("3");
        blackColorSelection.click();
        submitProduct.click();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        ///After submitting, click on "Continue Shopping" button
        driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span")).click();

        ///Try on different color
        whiteColorSelection.click();
        submitProduct.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span")).click();

        ///Click on the "Women" tab
        driver.findElement(By.linkText("Women")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        ///Browse and add one more product
        driver.findElement(By.linkText("Printed Dress")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        ///Since we tested on most of the feature, skip straight to order form
        Select itemSize2 = new Select(driver.findElement(By.xpath("//*[@id=\"group_1\"]")));
        itemSize2.selectByValue("2");

        ///Wishlist
        driver.findElement(By.xpath("//*[@id=\"wishlist_button\"]")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id=\"product\"]/div[2]/div/div/a")).click();

        driver.findElement(By.xpath("//*[@id=\"add_to_cart\"]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a/span")).click();

        Thread.sleep(3000);

        ///Managing cart
        /*
        WebElement qtyCart = driver.findElement(By.xpath("//*[@id=\"product_2_11_0_429424\"]/td[5]/input[2]"));
        qtyCart.clear();
        qtyCart.sendKeys("2");



        Thread.sleep(2000);

        ///Deleting second item in cart
        driver.findElement(By.xpath("//*[@id=\"2_12_0_429424\"]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"center_column\"]/p[2]/a[1]")).click();


        WebElement accountButton = driver.findElement(By.xpath("//*[@class='account']"));

        ///Checking sign in status, will log in if not yet
        if (accountButton.isDisplayed() == false){
            driver.findElement(By.id("email")).sendKeys("aepgais@gamail.com");
            driver.findElement(By.id("passwd")).sendKeys("password");
            Thread.sleep(2000);
            driver.findElement(By.id("SubmitLogin")).click();
        }

         */

        ///Proceed to Checkout after picking address
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.linkText("Proceed to checkout")).click();

        ///Confirming Address
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"center_column\"]/form/p/button")).click();


        ///Check the checkbox for Terms of Service, and then go to the next step
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.name("cgv")).click();
        driver.findElement(By.name("processCarrier")).click();

        ///Choosing payment method
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@class='bankwire']")).click();

        ///Confirming order and then go back to orders
        Thread.sleep(4000);
        driver.findElement(By.xpath("//*[@id=\"cart_navigation\"]/button")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id=\"center_column\"]/p/a")).click();

        Thread.sleep(5000);

        ///Sign out so we can check the login page
        driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[2]/a")).click();
    }

    @Test(priority = 2)
    public void loginAndAccountPage() throws InterruptedException {

        ///Since we need new random email before running the script, don't forget to change the email field

        driver.getCurrentUrl();
        if (driver.getCurrentUrl() != "http://automationpractice.com/index.php?controller=authentication&back=my-account"){
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.findElement(By.xpath("//*[@class='login']")).click();
        }

        ///Input log in data
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.id("email")).sendKeys("lee@doo.com");
        driver.findElement(By.id("passwd")).sendKeys("password");
        driver.findElement(By.id("SubmitLogin")).click();

        ///Checking every tabs at "My Account" tab

        ///Order History
        driver.findElement(By.partialLinkText("ORDER HISTORY")).click();
        Thread.sleep(3000);
        driver.findElement(By.partialLinkText("Back")).click();

        ///Credit Slips
        driver.findElement(By.partialLinkText("CREDIT SLIPS")).click();
        Thread.sleep(2000);
        driver.findElement(By.partialLinkText("Back")).click();

        ///Addresses

        driver.findElement(By.partialLinkText("ADDRESSES")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        ///Adding new address
        driver.findElement(By.partialLinkText("new address")).click();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("address1")).sendKeys("Grove Street");
        driver.findElement(By.id("address2")).sendKeys("Number 46");
        driver.findElement(By.id("city")).sendKeys("Los Santos");
        Select otherAddress = new Select(driver.findElement(By.id("id_state")));
        otherAddress.selectByValue("46");
        driver.findElement(By.id("postcode")).sendKeys("91234");
        driver.findElement(By.id("phone")).sendKeys("123321");
        driver.findElement(By.id("phone_mobile")).sendKeys("234432");
        driver.findElement(By.id("submitAddress")).click();

        ///Deleting one address
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id=\"center_column\"]/div[1]/div/div[2]/ul/li[9]/a[2]")).click();

        ///Switching to popup
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.switchTo().alert().accept();
        Thread.sleep(2000);
        driver.findElement(By.partialLinkText("Back")).click();

        ///Personal Info, try changing password
        driver.findElement(By.partialLinkText("PERSONAL INFORMATION")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.id("old_passwd")).sendKeys("password");
        driver.findElement(By.id("passwd")).sendKeys("wordpass");
        driver.findElement(By.id("confirmation")).sendKeys("wordpass");
        driver.findElement(By.name("submitIdentity")).click();

        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        driver.findElement(By.partialLinkText("Back")).click();

        ///Reenter personal info to change the password back
        driver.findElement(By.partialLinkText("PERSONAL INFORMATION")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.id("old_passwd")).sendKeys("wordpass");
        driver.findElement(By.id("passwd")).sendKeys("password");
        driver.findElement(By.id("confirmation")).sendKeys("password");
        driver.findElement(By.name("submitIdentity")).click();

        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        driver.findElement(By.partialLinkText("Back")).click();

        ///Wishlist
        driver.findElement(By.xpath("//*[@id=\"center_column\"]/div/div[2]/ul/li/a")).click();
        Thread.sleep(2000);
        driver.findElement(By.partialLinkText("Back")).click();

        ///Signout
        driver.findElement(By.xpath("//*[@class='logout']")).click();
    }

    @AfterTest
    public void closeBrowser(){
        driver.quit();
    }


}
